% [mu_est,sigma_est,p_est]=gaussian_mixture_model(c',3)

a = importdata('temphelix1');
hist(a,bins)
afold = 3.0;
b = importdata('temphelix2');
hist(b,bins)
bfold = 3.0;
c = importdata('temphelix3');
hist(c,bins)
cfold = 3.0;

rmsd = importdata('tempall');


% Define the 0-point when all the helices are unfolded
all_unfolded = intersect(find(rmsd>3),intersect(find(c > cfold),intersect(find(b > bfold),find(a > afold))));

lastTime = 0;
nanosecondsTilFolded = [];
for i=1:length(all_unfolded)
    if all_unfolded(i) < lastTime
        continue
    end
    j=all_unfolded(i);
    while j < length(rmsd) && rmsd(j) > 3.5
        j = j + 1;
    end
    lastTime = j;
    nanosecondsTilFolded = [nanosecondsTilFolded; (lastTime-all_unfolded(i))*0.2];
end
hist(nanosecondsTilFolded/1000)
disp(sprintf('Number of transitions: %d',length(nanosecondsTilFolded)))
disp(sprintf('Microseconds to fold from unfolded: %2.1f +/- %2.1f',mean(nanosecondsTilFolded)/1000,std(nanosecondsTilFolded)/1000));




% Define the 0-point when all the helices are folded
all_unfolded = intersect(find(rmsd>6),intersect(find(c < 1.5),intersect(find(b < 1.5),find(a < 1.5))));

lastTime = 0;
nanosecondsTilFolded = [];
for i=1:length(all_unfolded)
    if all_unfolded(i) < lastTime
        continue
    end
    j=all_unfolded(i);
    while j < length(rmsd) && rmsd(j) > 3.5
        j = j + 1;
    end
    lastTime = j;
    nanosecondsTilFolded = [nanosecondsTilFolded; (lastTime-all_unfolded(i))*0.2];
end
hist(nanosecondsTilFolded/1000)
disp(sprintf('Number of transitions: %d',length(nanosecondsTilFolded)))
disp(sprintf('Microseconds to fold from unfolded: %2.1f +/- %2.1f',mean(nanosecondsTilFolded)/1000,std(nanosecondsTilFolded)/1000));




all_unfolded1 = intersect(find(c > cfold),intersect(find(b > bfold),find(a < afold)));
all_unfolded2 = intersect(find(c > cfold),intersect(find(b < bfold),find(a > afold)));
all_unfolded3 = intersect(find(c < cfold),intersect(find(b > bfold),find(a > afold)));
all_unfolded = union(all_unfolded1,union(all_unfolded2,all_unfolded3));
lastTime = 0;
nanosecondsTilFolded = [];
for i=1:length(all_unfolded)
    if all_unfolded(i) < lastTime
        continue
    end
    j=all_unfolded(i);
    while j < length(rmsd) && rmsd(j) > 3.5
        j = j + 1;
    end
    lastTime = j;
    nanosecondsTilFolded = [nanosecondsTilFolded; (lastTime-all_unfolded(i))*0.2];
end
hist(nanosecondsTilFolded/1000)
disp(sprintf('Number of transitions: %d',length(nanosecondsTilFolded)))
disp(sprintf('Microseconds to fold from unfolded: %2.1f +/- %2.1f',mean(nanosecondsTilFolded)/1000,std(nanosecondsTilFolded)/1000));







all_unfolded1 = intersect(find(rmsd>6),intersect(find(c > cfold),intersect(find(b < bfold),find(a < afold))));
all_unfolded2 = intersect(find(rmsd>6),intersect(find(c < cfold),intersect(find(b < bfold),find(a > afold))));
all_unfolded3 = intersect(find(rmsd>6),intersect(find(c < cfold),intersect(find(b > bfold),find(a < afold))));
all_unfolded = union(all_unfolded1,union(all_unfolded2,all_unfolded3));
lastTime = 0;
nanosecondsTilFolded = [];
for i=1:length(all_unfolded)
    if all_unfolded(i) < lastTime
        continue
    end
    j=all_unfolded(i);
    while j < length(rmsd) && rmsd(j) > 3.5
        j = j + 1;
    end
    lastTime = j;
    nanosecondsTilFolded = [nanosecondsTilFolded; (lastTime-i)*0.2];
end
hist(nanosecondsTilFolded/1000)
disp(sprintf('Number of transitions: %d',length(nanosecondsTilFolded)))
disp(sprintf('Microseconds to fold from unfolded: %2.1f +/- %2.1f',mean(nanosecondsTilFolded)/1000,std(nanosecondsTilFolded)/1000));



